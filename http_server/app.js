var configs = require('../configs');

var db = require('../utils/db');
db.init(configs.mysql());

var redis = require('../utils/redis');
redis.init(configs.redis());

var info_service = require('./http_service');
info_service.start(configs.http_server());

