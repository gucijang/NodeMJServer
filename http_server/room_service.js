'use strict';

var crypto = require('../utils/crypto');
var db = require('../utils/db');
var redis = require("../utils/redis");
var http = require('../utils/http');
var constant = require('../utils/constant');

var serverMap = {};
var ROOM_PRI_KEY = "";
exports.serverMap = serverMap;
exports.ROOM_PRI_KEY = ROOM_PRI_KEY;

//token的意义在于不同的时间点会变化
exports.createRoom = function(userId, conf, callback){
    //roomNo roomId唯一性
    //rpc创建房间
    //更新缓存用户房间信息
    var serverInfo = chooseServer();
    if(!serverInfo){
        callback(constant.CODE_GAMESERVER_ERROR, null);
        return;
    }

    var param = {
        userId:userId,
        conf:JSON.stringify(conf),
    };
    param.sign = crypto.md5(userId + exports.ROOM_PRI_KEY);
    http.get(serverInfo.ip, serverInfo.httpPort, "/create_room", param, function (code, ret) {
        if (ret) {
            if(ret.code == constant.CODE_SUCCESS){
                var rs = {
                    roomNo: ret.roomNo,
                    conf: param.conf,
                    creator: userId,

                    ip: serverInfo.ip,
                    httpPort: serverInfo.httpPort,
                    socketPort: serverInfo.socketPort,
                };
                rs.sign = crypto.md5(String(ret.roomNo) + String(param.userId) + exports.ROOM_PRI_KEY);

                //缓存用户的房间号
                redis.cacheUserRoomNo(param.userId, ret.roomNo);

                //缓存房间信息
                var roomInfo = {
                    roomId: ret.roomId,
                    roomNo: ret.roomNo,
                    conf: param.conf,
                    ip: serverInfo.ip,
                    httpPort: serverInfo.httpPort,
                    socketPort: serverInfo.socketPort,
                    creator: userId,
                    createTime: ret.createTime,
                    members: 0,
                };
                redis.cacheRoomInfo(ret.roomNo, roomInfo);
                db.setRoomInfo(roomInfo, null);
                callback(constant.CODE_SUCCESS, rs);
            } else{
                callback(ret.code, null);
            }
            return;
        }
        callback(code, null);
        return;
    });
};

exports.enterRoom = function(userId, roomNo, callback){
    //获取房间信息
    //rpc创建房间
    //更新缓存用户房间信息
    redis.getRoomInfo(roomNo, function (object) {
        if (!object){ //没有房间号的缓存信息
            callback(constant.CODE_REDIS_EMPTY_ERROR, null);
            return;
        }
        var id = object.ip + ":" + object.httpPort;
        var serverInfo = serverMap[id];
        if(!serverInfo){
            callback(constant.CODE_GAMESERVER_ERROR, null);
            return;
        }
        rpc_enterRoom(serverInfo);
    });

    var rpc_enterRoom = function (serverInfo) {
        var params = {
            userId:userId,
            roomNo:roomNo,
        };
        params.sign = crypto.md5(roomNo + exports.ROOM_PRI_KEY);
        http.get(serverInfo.ip, serverInfo.httpPort, "/enter_room", params, function(code, ret){
            if(ret){
                if(ret.code == constant.CODE_SUCCESS){
                    var rs = {
                        roomNo: params.roomNo,
                        conf: ret.conf,
                        creator: ret.creator,

                        ip: serverInfo.ip,
                        httpPort: serverInfo.httpPort,
                        socketPort: serverInfo.socketPort,
                    };
                    rs.sign = crypto.md5(params.roomNo + String(params.userId) + exports.ROOM_PRI_KEY);

                    //缓存用户的房间号
                    redis.cacheUserRoomNo(params.userId, params.roomNo);

                    callback(constant.CODE_SUCCESS, rs);
                } else{
                    callback(ret.code, null);
                }
                return;
            }
            callback(code, null);
            return;
        });
    }
};

function chooseServer(){
    var serverInfo = null;
    for(var s in serverMap) {
        var info = serverMap[s];
        if(serverInfo == null){
            serverInfo = info;
        } else { //选房间数量最少的的服务器
            if(serverInfo.load > info.load){
                serverInfo = info;
            }
        }
    }
    return serverInfo;
}