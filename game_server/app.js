var configs = require('../configs');

var db = require('../utils/db');
db.init(configs.mysql());

var redis = require('../utils/redis');
redis.init(configs.redis());

var http_service = require("./rpc_service");
var socket_service = require("./socket_service");
var config = configs.game_server();

http_service.start(config);
socket_service.start(config);
