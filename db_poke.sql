/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : db_poke

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2017-03-30 20:09:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `t_users`
-- ----------------------------
DROP TABLE IF EXISTS `t_users`;
CREATE TABLE `t_users` (
  `user_id` int(11) unsigned NOT NULL COMMENT '用户ID 6位数开始',
  `unionid` varchar(64) NOT NULL DEFAULT '' COMMENT '账号',
  `account` varchar(64) NOT NULL DEFAULT '' COMMENT '账号',
  `name` varchar(64) DEFAULT NULL COMMENT '用户昵称',
  `sex` tinyint(2) DEFAULT NULL COMMENT '性别',
  `headimg` varchar(256) DEFAULT NULL COMMENT '头像',
  `ip` varchar(64) NOT NULL COMMENT 'ip',
  `province` varchar(64) NOT NULL COMMENT '省',
  `city` varchar(64) NOT NULL COMMENT '市',
  `coins` int(11) DEFAULT '0' COMMENT '用户金币',
  `gems` int(11) DEFAULT '0' COMMENT '用户宝石',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',
  `update_time` bigint(20) COMMENT '更新时间',

  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Records of t_users
-- ----------------------------


-- ----------------------------
-- Table structure for `t_user_room`
-- ----------------------------
DROP TABLE IF EXISTS `t_user_room`;
CREATE TABLE `t_user_room` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID 6位数开始',
  `room_id` bigint(20) NOT NULL COMMENT '房间唯一ID user_id+timestamp',
  `create_time` bigint(20) NOT NULL COMMENT '创建时间',

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Records of t_user_room
-- ----------------------------

-- ----------------------------
-- Table structure for `t_rooms`
-- ----------------------------
DROP TABLE IF EXISTS `t_rooms`;
CREATE TABLE `t_rooms` (
  `room_id` bigint(20) unsigned NOT NULL COMMENT '房间唯一ID user_id+timestamp',
  `room_no` VARCHAR(8) NOT NULL COMMENT '房间号6位',
  `conf` varchar(256) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `http_port` varchar(8) NOT NULL,
  `socket_port` varchar(8) NOT NULL,
  `creator` int(11) NOT NULL,
  `create_time` bigint(20) NOT NULL,
  `members` tinyint(2) DEFAULT 0,

  `plays_info` varchar(1024) DEFAULT NULL,
  `result` varchar(256) DEFAULT NULL,

  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_rooms
-- ----------------------------

-- ----------------------------
-- Table structure for `t_games_archive`
-- ----------------------------
-- DROP TABLE IF EXISTS `t_games_archive`;
-- CREATE TABLE `t_games_archive` (
--   `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
--   `room_id` bigint(20) unsigned NOT NULL,
--   `game_index` smallint(6) NOT NULL,
--   `conf` varchar(256) NOT NULL,
--   `result` char(255) DEFAULT NULL,
--   `create_time` bigint(20) NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_games_archive
-- ----------------------------

-- ----------------------------
-- Table structure for `t_message`
-- ----------------------------
DROP TABLE IF EXISTS `t_message`;
CREATE TABLE `t_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` tinyint(2) NOT NULL,
  `msg` varchar(1024) NOT NULL,
  `version` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_message
-- ----------------------------



