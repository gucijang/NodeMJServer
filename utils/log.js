var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';

function debug (msg, data) {
    logger.debug(msg + '--------' + data);
};

function info (msg, data) {
    logger.info(msg + '--------' + data);
};

function error (msg, data) {
    logger.error(msg + '--------' + data);
};

exports.debug = debug;
exports.info = info;
exports.error = error;



