'use strict';

var redis = require('redis');
var logger = require('../utils/log');

var redisClient = null;
var user_id = 100000;

function init(config){
    redisClient = redis.createClient({
        host: config.HOST,
        port: config.PORT,
        // password: config.PSWD,
    });

    redisClient.on('error', function (err) {
        logger.error('redis', err);
    });

    //获取6位数的userId
    redisClient.GET(REDIS_USER_ID, function (err, object) {
        if (err == null) {
            user_id = parseInt(object);
        }
    });
};

//6位数的userId
function getUserId() {
    user_id++;
    return user_id;
};

//6位数的userId
function cacheUserId() {
    redisClient.SET(REDIS_USER_ID, String(user_id));
};

//用户授权信息
function getUserAuthInfo(openid, callback) {
    var key = redisCacheKey(REDIS_USER_AUTH_INFO, openid);
    redisClient.HGETALL(key, function (err, object) {
        if (err == null) {
            callback(object);
        }
    });
};

//用户授权信息
function cacheUserAuthInfo(openid, authInfo) {
    var key = redisCacheKey(REDIS_USER_AUTH_INFO, openid);
    redisClient.HMSET(key, authInfo);
};

//用户信息
function getUserInfo(userId, callback) {
    var key = redisCacheKey(REDIS_USER_INFO, userId);
    redisClient.HGETALL(key, function (err, object) {
        if (err == null) {
            callback(object);
        }
    });
};

//用户信息
function cacheUserInfo(userId, userInfo) {
    var key = redisCacheKey(REDIS_USER_INFO, userId);
    redisClient.HMSET(key, userInfo);
};

//用户信息
function getUserInfo(userId, callback) {
    var key = redisCacheKey(REDIS_USER_INFO, userId);
    redisClient.HGETALL(key, function (err, object) {
        if (err == null) {
            callback(object);
        }
    });
};

//批量获取用户信息(查找房间玩家信息)
function getSomeUserInfo(users, callback) {
    var multi = redisClient.multi();
    var key = null;
    users.forEach(function (userId, index) {
        key = redisCacheKey(REDIS_USER_INFO, userId);
        multi.HGETALL(key);
    });
    multi.exec(function (err, replies) {
        callback(replies);
    });
};

//用户房间信息
function cacheUserRoomNo(userId, roomNo) {
    var key = redisCacheKey(REDIS_USER_ROOM_INFO, userId);
    redisClient.SADD(key, roomNo);
};

//移除用户房间号
function removeUserRoomNo(userId, roomNo) {
    var key = redisCacheKey(REDIS_USER_ROOM_INFO, userId);
    redisClient.SREM(key, roomNo);
};

//批量移除用户房间号
function removeUserAllRoomNo(userIds, roomNo) {
    var multi = redisClient.multi();
    var key = null;
    userIds.forEach(function (userId, index) {
        key = redisCacheKey(REDIS_USER_ROOM_INFO, userId);
        multi.SREM(key, roomNo);
    });
    multi.exec(null);
};

//用户相关的所有房间信息
function getUserRoomInfo(userId, callback) {
    var key = redisCacheKey(REDIS_USER_ROOM_INFO, userId);
    redisClient.SMEMBERS(key, function (err, list) {
        if (list) {
            var multi = redisClient.multi();
            var key = null;
            list.forEach(function (roomNo, index) {
                key = redisCacheKey(REDIS_ROOM_INFO, roomNo);
                multi.HGETALL(key);
            });
            multi.exec(function (err, replies) {
                callback(replies);
            });
        } else  {
            callback(null);
        }
    });
};

//缓存房间信息
function cacheRoomInfo(roomNo, roomInfo) {
    var key = redisCacheKey(REDIS_ROOM_INFO, roomNo);
    redisClient.HMSET(key, roomInfo);
};

//获取房间信息
function getRoomInfo(roomNo, callback) {
    var key = redisCacheKey(REDIS_ROOM_INFO, roomNo);
    redisClient.HGETALL(key, function (err, object) {
        if (err == null) {
            callback(object);
        }
    });
};

//删除房间信息
function removeRoomInfo(roomNo, callback) {
    var key = redisCacheKey(REDIS_ROOM_INFO, roomNo);
    redisClient.DEL(key, callback);
};

//更改房间人员数量
function incrRoomMembers(roomNo, incr) {
    var key = redisCacheKey(REDIS_ROOM_INFO, roomNo);
    redisClient.HINCRBY(key, 'members', incr);
};

//获取所有的房间信息
function getAllRoomInfo(callback) {
    redisClient.KEYS(REDIS_ROOM_INFO, function (err, list) {
        if (list) {
            var multi = redisClient.multi();
            list.forEach(function (key) {
                multi.HGETALL(key);
            });
            multi.exec(function (err, replies) {
                callback(replies);
            });
        }
    });
};

//缓存房间麻将结果
function cacheRoomGameResult(roomNo, resultStr) {
    var key = redisCacheKey(REDIS_ROOM_RESULT_INFO, roomNo);
    redisClient.SET(key, resultStr);
};

//移除房间麻将结果
function removeRoomGameResult(roomNo) {
    var key = redisCacheKey(REDIS_ROOM_RESULT_INFO, roomNo);
    redisClient.DEL(key);
};

//获取房间麻将结果
function getRoomGameResults(roomNo, callback) {
    var key = redisCacheKey(REDIS_ROOM_RESULT_INFO, roomNo);
    redisClient.GET(key, function (err, result) {
        if (err == null) {
            callback(result);
        }
    });
};

function removeRoomAndGameResult(roomNo) {
    var multi = redisClient.multi();
    var key1 = redisCacheKey(REDIS_ROOM_INFO, roomNo);
    multi.DEL(key1);

    var key2 = redisCacheKey(REDIS_ROOM_RESULT_INFO, roomNo);
    multi.DEL(key2);

    multi.exec(null);
};

function redisCacheKey(key, objectId) {
    return key + objectId;
};

exports.cacheUserId = cacheUserId;
exports.getUserId = getUserId;

exports.cacheUserAuthInfo = cacheUserAuthInfo;
exports.getUserAuthInfo = getUserAuthInfo;

exports.cacheUserInfo = cacheUserInfo;
exports.getUserInfo = getUserInfo;
exports.getSomeUserInfo = getSomeUserInfo;

exports.cacheUserRoomNo = cacheUserRoomNo;
exports.removeUserRoomNo = removeUserRoomNo;
exports.removeUserAllRoomNo = removeUserAllRoomNo;
exports.getUserRoomInfo = getUserRoomInfo;

exports.cacheRoomInfo = cacheRoomInfo;
exports.getRoomInfo = getRoomInfo;
exports.removeRoomInfo = removeRoomInfo;
exports.incrRoomMembers = incrRoomMembers;

exports.getAllRoomInfo = getAllRoomInfo;

exports.cacheRoomGameResult = cacheRoomGameResult;
exports.removeRoomGameResult = removeRoomGameResult;
exports.getRoomGameResults = getRoomGameResults;

exports.removeRoomAndGameResult = removeRoomAndGameResult;

exports.init = init;
exports.redisCacheKey = redisCacheKey;

const REDIS_ROOM_INFO = "REDIS:ROOM:";
const REDIS_ROOM_RESULT_INFO = "REDIS:ROOM:RESULT:";
const REDIS_USER_ID = "REDIS:USERID";
const REDIS_USER_INFO = "REDIS:USER:";
const REDIS_USER_AUTH_INFO = "REDIS:USER:AUTH";
const REDIS_USER_ROOM_INFO = "REDIS:USER:ROOM:";

// exports.REDIS_ROOM_INFO = REDIS_ROOM_INFO;
// exports.REDIS_ROOM_RESULT_INFO = REDIS_ROOM_RESULT_INFO;
// exports.REDIS_USER_INFO = REDIS_USER_INFO;
// exports.REDIS_USER_ROOM_INFO = REDIS_USER_ROOM_INFO;

