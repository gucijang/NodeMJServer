﻿var SERVER_IP = "127.0.0.1";
var MYSQL_IP = "119.23.220.70";
var REDIS_IP = "127.0.0.1";

// var SERVER_IP = "119.23.220.70";
// var MYSQL_IP = "119.23.220.70";
// var REDIS_IP = "127.0.0.1";

var HTTP_SERVER_HTTP_PORT = 9000;
var GAME_SERVER_HTTP_PORT = 9001;
var GAME_SERVER_SOCKET_PORT = 10000;

var ACCOUNT_PRI_KEY = "^&*#$%()2@";
var ROOM_PRI_KEY = "~!@#$(*&^%$2&";

//账号服配置
exports.http_server = function(){
    return {
        SERVER_IP: SERVER_IP,
        SERVER_PORT: HTTP_SERVER_HTTP_PORT,

        ACCOUNT_PRI_KEY:ACCOUNT_PRI_KEY,
        ROOM_PRI_KEY:ROOM_PRI_KEY,
        VERSION:'20170727',
    };
};

//游戏服配置
exports.game_server = function(){
    return {
        SERVER_IP: SERVER_IP,
        SERVER_PORT: GAME_SERVER_HTTP_PORT,
		SOCKET_PORT: GAME_SERVER_SOCKET_PORT,

        //服务器编号
        SERVER_ID:"001",
        HTTP_TICK_TIME:15000,

        ACCOUNT_PRI_KEY:ACCOUNT_PRI_KEY,
        ROOM_PRI_KEY:ROOM_PRI_KEY,

		//http server
        HTTP_SERVER_SERVER_IP: SERVER_IP,
        HTTP_SERVER_SERVER_PORT: HTTP_SERVER_HTTP_PORT,
    };
};

//mysql配置
exports.mysql = function(){
    return {
        HOST:MYSQL_IP,
        USER:'root',
        PSWD:'root',
        DB:'db_poke',
        PORT:3306,
    }
};

exports.redis = function(){
    return {
        HOST:REDIS_IP,
        PSWD:'root',
        PORT:6379,
    }
};

